package com.example.member;


import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.id;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class PointActivity extends Fragment {
	Button kadaluarsaBT, jumlahpoinBT,detailBT;
	Activity activity;
	TextView penjelasantv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_point);
		View rootView = inflater.inflate(R.layout.activity_point, container, false);
		jumlahpoinBT = (Button)rootView.findViewById(R.id.jumlahpoinbt);
		kadaluarsaBT = (Button)rootView.findViewById(R.id.kadaluarsabt);
		detailBT = (Button)rootView.findViewById(R.id.detailbt);
		penjelasantv=(TextView)rootView.findViewById(id.textView2);
//		activity=this;
		penjelasantv.setText(CustomHttpClient.info(getActivity(),"penjelasanpoint.php"));
		jumlahpoinBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("PointActivity", "JumlahPoint button");
				Intent intent = new Intent(activity, JumlahPoinActivity.class);
				startActivity(intent);
			}
		});
		detailBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("PointActivity", "Detail button");
				Intent intent = new Intent(activity, DetailTransaksiActivity.class);
				startActivity(intent);
			}
		});
		kadaluarsaBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("PointActivity", "Poin Kadaluarsa button");
				Intent intent = new Intent(activity, PoinKadaluarsaActivity.class);
				startActivity(intent);
			}
		});
		return rootView;
	}


}
