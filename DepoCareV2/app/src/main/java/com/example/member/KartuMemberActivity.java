package com.example.member;


import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.id;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class KartuMemberActivity extends Activity {
	Activity activity;
	Button kartumemberBT,carikartumemberBT,benefitmemberBT;
	TextView penjelasanTV;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kartumember);
		kartumemberBT = (Button)findViewById(R.id.button1);
		benefitmemberBT = (Button)findViewById(R.id.button3);
		penjelasanTV = (TextView)findViewById(id.textView2);
		activity=this;
		penjelasanTV.setText(CustomHttpClient.info(this,"penjelasankartumember.php"));
		kartumemberBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("KartuMemberActivity", "KartuMember button");
				Intent intent = new Intent(activity, LoginRegistrasiActivity.class);
				startActivity(intent);
			}
		});
		
		benefitmemberBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("KartuMemberActivity", "benefitMember button");
				Intent intent = new Intent(activity, BenefitActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.member, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
