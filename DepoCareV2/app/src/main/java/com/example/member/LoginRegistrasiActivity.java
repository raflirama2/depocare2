package com.example.member;

import android.app.Activity;
import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.depocarev2.R;

public class LoginRegistrasiActivity extends Activity {
    Button signupBT,signinBT;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_registrasi);
        signupBT =(Button)findViewById(R.id.button1);
        signinBT =(Button)findViewById(R.id.button2);
        activity=this;
        signupBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("LoginRegistrasi", "signup button");
                Intent intent = new Intent(activity, RegistrasiActivity.class);
                startActivity(intent);
                finish();
            }
        });
        signinBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("LoginRegistrasi", "signup button");
                Intent intent = new Intent(activity, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
