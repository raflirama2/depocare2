package com.example.member;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.id;

import android.app.Activity;
import android.content.Intent;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class BenefitActivity extends Activity {

	Button benefitBT,ketentuanBT,FAQBT;
	Activity activity;
	TextView penejelasantv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_benefit);
		benefitBT = (Button)findViewById(R.id.button1);
		ketentuanBT= (Button)findViewById(R.id.button2);
		FAQBT = (Button)findViewById(R.id.button3);
		penejelasantv = (TextView)findViewById(id.textView2);
		penejelasantv.setText(CustomHttpClient.info(this,"penjelasanbenefitdanketentuankartumember.php"));

		activity=this;
		benefitBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("BenefitActivity", "benefit button");
				Intent intent = new Intent(activity, BenefitInfoActivity.class);
				intent.putExtra("info","BENEFIT KARTU MEMBER" );
				startActivity(intent);
			}
		});
		ketentuanBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("BenefitActivity", "ketentuan button");
				Intent intent = new Intent(activity, BenefitInfoActivity.class);
				intent.putExtra("info","KETENTUAN KARTU MEMBER" );
				startActivity(intent);
			}
		});
		FAQBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("BenefitActivity", "FAQ button");
				Intent intent = new Intent(activity, BenefitInfoActivity.class);
				intent.putExtra("info","FAQ" );
				startActivity(intent);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.benefit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
