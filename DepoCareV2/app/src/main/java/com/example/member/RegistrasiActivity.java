package com.example.member;

//import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.depocarev2.R;

public class RegistrasiActivity extends Activity {
    Button signupBT;
    Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        signupBT = (Button)findViewById(R.id.button);
        activity=this;
        signupBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("RegistrasiActivity", "signUP button");
                Intent intent = new Intent(activity, LoginRegistrasiActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
