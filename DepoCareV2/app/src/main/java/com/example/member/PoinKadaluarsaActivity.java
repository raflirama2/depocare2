package com.example.member;

//import android.support.v7.app.ActionBarActivity;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.id;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PoinKadaluarsaActivity extends Fragment {
	TextView penjelasantv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.activity_poin_kadaluarsa, container, false);
		penjelasantv = (TextView)rootView.findViewById(id.textView3);
		penjelasantv.setText(CustomHttpClient.info(getActivity(),"penjelasanpointkadaluarsa.php"));
		return rootView;
	}


}
