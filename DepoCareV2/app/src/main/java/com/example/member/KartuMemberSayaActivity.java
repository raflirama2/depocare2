package com.example.member;


import java.util.EnumMap;
import java.util.Map;

import com.example.depocarev2.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.*;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class KartuMemberSayaActivity extends Fragment {
	Button pointBT, profilBT,testBT;
	TextView memberTV;
	EditText testET;
	Activity activity;
	ImageView iv,logo;
	String Member;
	public KartuMemberSayaActivity(Activity activity,String member){
		Member=member;
	}
	
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.activity_kartumembersaya, container, false);
//		Intent intent = getIntent();
//		String member=intent.getStringExtra("member");
//		String ktp=intent.getStringExtra("ktp");
//		pointBT = (Button)rootView.findViewById(R.id.pointbt);
//		profilBT = (Button)rootView.findViewById(R.id.profilbt);
//		testBT = (Button)rootView.findViewById(R.id.button1);
//		testET=(EditText)rootView.findViewById(R.id.editText1);

		iv=(ImageView)rootView.findViewById(R.id.imageView3);
		logo=(ImageView)rootView.findViewById(R.id.imageView2);

		memberTV = (TextView)rootView.findViewById(R.id.textView10);
		String barcode_data=Member;
		String code = Member.substring(0,2);
		if(code.equals("01")){
			logo.setImageResource(R.drawable.dc_db01);
		}
		else if(code.equals("02")){
			logo.setImageResource(R.drawable.dc_db02);
		}
		else if(code.equals("03")){
			logo.setImageResource(R.drawable.dc_db03);
		}
		else if(code.equals("04")){
			logo.setImageResource(R.drawable.dc_db04);
		}
		else if(code.equals("05")){
			logo.setImageResource(R.drawable.dc_db04);
		}
		else if(code.equals("05")){
			logo.setImageResource(R.drawable.dc_db05);
		}
		else if(code.equals("06")){
			logo.setImageResource(R.drawable.dc_db06);
		}
		memberTV.setText(Member);
		Bitmap bitmap=null;
		try {
			if(!barcode_data.equals(null)){
				bitmap = encodeAsBitmap(barcode_data, BarcodeFormat.ITF, 600, 200);
				iv.setImageBitmap(bitmap);
			}

		} catch (WriterException e) {
			e.printStackTrace();
		}
		
//		pointBT.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						Log.d("KartuMemberSayaActivity", "Point button");
//						Intent intent = new Intent(activity, PointActivity.class);
//						startActivity(intent);
//					}
//				});
//		profilBT.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Log.d("KartuMemberSayaActivity", "Profil button");
//				Intent intent = new Intent(activity, ProfilActivity.class);
//				startActivity(intent);
//			}
//		});
//		testBT.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Log.d("KartuMemberSayaActivity", "Profil button");
//				String barcode_data = testET.getText().toString();
////				String barcode_data = "123456";
//				Bitmap bitmap = null;
//
//			    try {
//			    	if(!barcode_data.equals(null)){
//			        bitmap = encodeAsBitmap(barcode_data, BarcodeFormat.ITF, 600, 150);
//			        iv.setImageBitmap(bitmap);
//			    	}
//
//			    } catch (WriterException e) {
//			        e.printStackTrace();
//			    }
//
//			}
//		});
		return rootView;
	}
	//ini adalah proses barcode generator
	Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
	    String contentsToEncode = contents;
	    if (contentsToEncode == null) {
	        return null;
	    }
	    Map<EncodeHintType, Object> hints = null;
	    String encoding = guessAppropriateEncoding(contentsToEncode);
	    if (encoding != null) {
	        hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
	        hints.put(EncodeHintType.CHARACTER_SET, encoding);
	    }
	    MultiFormatWriter writer = new MultiFormatWriter();
	    BitMatrix result;
	    try {
	        result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
	    } catch (IllegalArgumentException iae) {
	        // Unsupported format
	        return null;
	    }
	    int width = result.getWidth();
	    int height = result.getHeight();
	    int[] pixels = new int[width * height];
	    for (int y = 0; y < height; y++) {
	        int offset = y * width;
	        for (int x = 0; x < width; x++) {
	        pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
	        }
	    }

	    Bitmap bitmap = Bitmap.createBitmap(width, height,
	        Bitmap.Config.ARGB_8888);
	    bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
	    return bitmap;
	}

	    private static String guessAppropriateEncoding(CharSequence contents) {
	    // Very crude at the moment
	    for (int i = 0; i < contents.length(); i++) {
	        if (contents.charAt(i) > 0xFF) {
	        return "UTF-8";
	        }
	    }
	    return null;
	    }

	}
