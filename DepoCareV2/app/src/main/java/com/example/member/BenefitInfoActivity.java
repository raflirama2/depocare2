package com.example.member;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BenefitInfoActivity extends Fragment {
	String judul;
	public BenefitInfoActivity(String Judul){
		judul=Judul;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.activity_benefit_info, container, false);
		TextView Title,Value;
//		Title=(TextView)rootView.findViewById(R.id.textView1);
		Value=(TextView)rootView.findViewById(R.id.textView2);
//		Title.setText(judul);
		if(judul.equals("BENEFIT KARTU MEMBER")){
			Value.setText(CustomHttpClient.info(getActivity(),"penjelasanbenefitkartumember.php"));
		}
		else if (judul.equals("KETENTUAN KARTU MEMBER")){
			Value.setText(CustomHttpClient.info(getActivity(),"penjelasanketentuankartumember.php"));
		}
		else if (judul.equals("FAQ")){
			Value.setText(CustomHttpClient.info(getActivity(),"penjelasanfaq.php"));
		}
		return rootView;
	}


}
