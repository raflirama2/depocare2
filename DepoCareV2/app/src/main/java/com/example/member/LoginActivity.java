package com.example.member;

import com.example.depocarev2.R;
import com.example.sidebar.SideBarMemberAcitvity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {
	EditText memberET,ktpET;
	Button GO;
	Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		memberET = (EditText)findViewById(R.id.memberField);		
		ktpET = (EditText)findViewById(R.id.ktpField);
		GO = (Button)findViewById(R.id.GOButton);
		
		memberET.setText("");
		ktpET.setText("");
		memberET.setHint("  Masukkan nomor Member anda");
		ktpET.setHint("  Masukkan Password anda");
		activity=this;
		
		GO.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("LoginActivity", "GO button");
				Intent intent = new Intent(activity, SideBarMemberAcitvity.class);
				intent.putExtra("member", memberET.getText().toString());
				intent.putExtra("ktp", ktpET.getText().toString());
				startActivity(intent);
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
