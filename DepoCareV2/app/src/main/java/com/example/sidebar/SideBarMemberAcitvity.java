package com.example.sidebar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.depocarev2.R;
import com.example.member.BenefitInfoActivity;
import com.example.member.DetailTransaksiActivity;
import com.example.member.JumlahPoinActivity;
import com.example.member.KartuMemberSayaActivity;
import com.example.member.LoginRegistrasiActivity;
import com.example.member.PoinKadaluarsaActivity;

public class SideBarMemberAcitvity extends AppCompatActivity {
    private String[] mNavigationDrawerItemTitles = {"Kartu Member Saya","Jumlah Poin","Detail Transaksi","Poin Kadaluarsa\nBulan ini",
            "Benefit Kartu Member","Ketentuan Kartu Member","FAQ","Log Out"};
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    Toolbar toolbar;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    ActionBarDrawerToggle mDrawerToggle;
    public Activity activity;
    ViewPager viewPager;
    int images[] = {R.drawable.tentang_depo, R.drawable.depo_all, R.drawable.main_background2, R.drawable.informasi_promosi_background};
    CustomViewPagerAdapter myCustomPagerAdapter;
    private final int delay = 2000;
    private int page = 0;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_side_bar_acitvity);
        mTitle = mDrawerTitle = getTitle();
//        mNavigationDrawerItemTitles= getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        setupToolbar();

        DataModel[] drawerItem = new DataModel[mNavigationDrawerItemTitles.length];

        drawerItem[0] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[0]);
        drawerItem[1] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[1]);
        drawerItem[2] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[2]);
        drawerItem[3] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[3]);
        drawerItem[4] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[4]);
        drawerItem[5] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[5]);
        drawerItem[6] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[6]);
        drawerItem[7] = new DataModel(R.drawable.ic_launcher, mNavigationDrawerItemTitles[7]);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();
        selectItem(0);

//        viewPager = (ViewPager)findViewById(R.id.view_pager);
//        myCustomPagerAdapter = new CustomViewPagerAdapter(this, images,this);
//        viewPager.setAdapter(myCustomPagerAdapter);
//
//        Runnable runnable = new Runnable() {
//            public void run() {
//                if (myCustomPagerAdapter.getCount() == page) {
//                    page = 0;
//                } else {
//                    page++;
//                }
//                viewPager.setCurrentItem(page, true);
//                handler.postDelayed(this, delay);
//            }
//        };
    }
    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }
    public void removeImage(){
        viewPager.setBackgroundColor(Color.WHITE);
    }

    public void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                Intent intent=getIntent();
                String member=intent.getStringExtra("member");
                fragment = new KartuMemberSayaActivity(activity,member);
                break;
            case 1:
                fragment = new JumlahPoinActivity();
                break;
            case 2:
                fragment = new DetailTransaksiActivity();
                break;
            case 3:
                fragment = new PoinKadaluarsaActivity();
                break;
            case 4:
                fragment = new BenefitInfoActivity("BENEFIT KARTU MEMBER");
                break;
            case 5:
                fragment = new BenefitInfoActivity("KETENTUAN KARTU MEMBER");
                break;
            case 6:
                fragment = new BenefitInfoActivity("FAQ");
                break;
            case 7:
                Intent intent1 = new Intent(this, LoginRegistrasiActivity.class);
                startActivity(intent1);
                finish();
                break;

            default:
                break;
        }

        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    void setupDrawerToggle(){
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }


}
