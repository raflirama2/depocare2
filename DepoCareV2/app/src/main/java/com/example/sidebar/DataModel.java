package com.example.sidebar;

/**
 * Created by CCTV on 05-Jun-17.
 */

public class DataModel {

    public int icon;
    public String name;

    // Constructor.
    public DataModel(int icon, String name) {

        this.icon = icon;
        this.name = name;
    }
}
