package com.example.depocarev2;

import com.example.member.LoginRegistrasiActivity;
import com.example.sidebar.SideBarInformasiAcitvity;

import android.app.Activity;
import android.content.Intent;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends Activity {
	Button memberButton,informasiButton;
	Activity activity;
	TextView penjelasanTV;
	String penjelasan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		memberButton = (Button)findViewById(R.id.memberButton);
		informasiButton = (Button)findViewById(R.id.informasiButton);
		penjelasanTV = (TextView)findViewById(R.id.textView4);
		activity=this;
//		ArrayList<NameValuePair> postParameter = new ArrayList<NameValuePair>();
//		postParameter.add(new BasicNameValuePair("test", "inisial"));
//		penjelasan = "OffLine";
//		try {
//			if (android.os.Build.VERSION.SDK_INT > 9) {
//				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//				StrictMode.setThreadPolicy(policy);
//			}
////			penjelasan = CustomHttpClient.executeHttpPost(CustomHttpClient.getIP()+"/depocare/penjelasanhome.php",postParameter,this,false);
//			penjelasan = CustomHttpClient.executeHttpPost(CustomHttpClient.getIP()+"/depocare/penjelasanhome.php",postParameter,this,false);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		penjelasan=CustomHttpClient.info(this,"penjelasanhome.php");

		penjelasanTV.setText(penjelasan.toString());


		memberButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("HomeActivity", "member button");
				Intent intent = new Intent(activity, LoginRegistrasiActivity.class);
				startActivity(intent);
			}
		});
		informasiButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("HomeActivity", "informasi button");
				Intent intent = new Intent(activity, SideBarInformasiAcitvity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
