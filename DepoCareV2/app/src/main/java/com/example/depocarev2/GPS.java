package com.example.depocarev2;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by CCTV on 05-Jun-17.
 */

public class GPS {
    public String getlocation2(Context context) throws IOException {
        String info="not active";
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Location location;

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}

            @Override
            public void onLocationChanged(Location location) {
                // TODO Auto-generated method stub

            }
        };

        // Register the listener with the Location Manager to receive location updates
        if(locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER)){
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }
        location=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Log.d("", "getlocation 1"+info+"location= "+location);
        if(location!=null){
            Log.d("", "getlocation 2"+info);
            info=getlocation(location.getLatitude(), location.getLongitude(),context);
            Log.d("", "getlocation 3"+info);
        }

        Log.d("", "Lokasi = "+info);
        return info;
    }

    public String getlocation(double latitude, double longitude,Context context) throws IOException{//using longitude latitude
        String info="Lokasi Belum dapat ditemukan";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
//		String state = addresses.get(0).getAdminArea()+"(provinsi)";
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

//		info=address+"\n"+city+"\n"+state+"\n"+country+"\n"+postalCode+"\n"+knownName;
        info=state;
        return info;
    }
}
