package com.example.informasipromosi;


import com.example.depocarev2.R;
import com.example.depocarev2.R.layout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class LokasGeraiActivity extends Fragment {
	
	Activity activity;
	String from;
	ListView list;
    String[] web = {
    		"  DEPO BANGUNAN KALIMALANG\n  Jalan Raya Tarum Barat No.46\n  Kalimalang Jakarta Timur 13440"
    		+ "\n  Telp. (62-21)865-2888\n  (Buka jam 07.00 - 20.00)",
            "  DEPO BANGUNAN TANGERANG\n  Jalan Raya Serpong Km.2 Pakulonan\n  Serpong Tangerang 15325\n  Telp. (62-21)5312-0808"
            + "\n  Jalan Raya Serpong Km.2 Pakulonan\n  Serpong Tangerang 15325\n  (Buka jam 07.00 - 20.00)",
            "  DEPO BANGUNAN SIDOARJO\n  Jalan Raya Ahmad Yani 41 - 43 \n  Gedangan, Kompleks Central Square\n  Waru Sidoarjo Jawa Timur 61254"
            + "\n  Telp. (62-31)855-7080,\n  (Buka jam 07.00 - 20.00)",
            "  DEPO BANGUNAN MALANG\n  Jalan Raya Karanglo No.69\n  Malang Singosari 65153\n  Telp. (62-341)482-888"
            + "\n  (Buka jam 07.00 - 20.00)",
            "  DEPO BANGUNAN BANDUNG\n  Jalan Soekarno Hatta, Cipamokolan,\n  Rancasari, Wilayah Gedebage 40292 "
            + "\n  Telp. (62-22)750-8999\n  (Buka jam 07.00 - 20.00)",
            "  DEPO BANGUNAN DENPASAR\n  Jalan Teuku Umar Barat No.388\n  Denpasar Bali 80117"
            + "\n  Telp. (62-361)847-5888\n  (Buka jam 07.00 - 20.00)",
            "  DEPO BANGUNAN BOGOR\n  Jalan KH. Soleh Iskandar (Jl. Baru)\n  Km 6.6 Kedung Badak\n  Tanah Sereal Bogor 16163\n  Telp. (62-251)755-8181"
            + "\n  WA & SMS. +6281-5986-1193\n  (Buka jam 07.30 - 20.30)"
    } ;
    Integer[] imageId = {
            R.drawable.depo_kalimalang,
            R.drawable.depo_tangerang,
            R.drawable.depo_sidoarjo,
            R.drawable.depo_malang,
            R.drawable.depo_bandung,
            R.drawable.depo_denpasar,
            R.drawable.depo_bogor
 
    };
    public LokasGeraiActivity(Activity activity,String from){
		this.activity=activity;
		this.from=from;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_lokas_gerai);
		View rootView = inflater.inflate(layout.activity_lokas_gerai, container, false);
		CustomList adapter = new CustomList(getActivity(), web, imageId);
        list=(ListView)rootView.findViewById(R.id.listView1);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
//        		Toast.makeText(LokasGeraiActivity.this, "You Clicked at " +web[+ position]+" "+position, Toast.LENGTH_SHORT).show();
        		if (from.equals("informasi")){
					Log.d("LokasGeraiActivity", "lokasi button" +web[+ position]);
					Intent intent = new Intent(getActivity(), LokasiGeraiDBActivity.class);
					intent.putExtra("lokasi", position);
					startActivity(intent);
				}
				else if (from.equals("informasipromosi"))
				{
					String lokasi="";
					Log.d("LokasGeraiActivity", "lokasi button" +web[+ position]);
					Intent intent = new Intent(activity, PromosiPenjualanActivity.class);
					if(position==0||position==1||position==4||position==6){
						lokasi="Jawa Barat";
					}
					else if(position==2||position==3){
						lokasi="Jawa Timur";
					}
					else if(position==5){
						lokasi="Bali";
					}
					intent.putExtra("lokasi", lokasi);
					startActivity(intent);
				}
			}
   		     });
		return rootView;
	}
}
