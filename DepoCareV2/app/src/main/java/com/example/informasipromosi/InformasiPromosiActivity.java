package com.example.informasipromosi;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.id;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
//import android.support.v7.app.ActionBarActivity;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class InformasiPromosiActivity extends Activity {

	Button informasiBT,PromosiBT;
	TextView penjelasanTV;
	Activity activity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_informasi_promosi);
		informasiBT = (Button) findViewById(R.id.button1);
		PromosiBT = (Button)findViewById(R.id.button2);
		penjelasanTV = (TextView)findViewById(id.textView2);
		activity=this;
		penjelasanTV.setText(CustomHttpClient.info(this,"penjelasaninformasidanpenjualan.php"));
		informasiBT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("InformasiPromosiActivity", "Informasi button");
				Intent intent = new Intent(activity, InformasiActivity.class);
				startActivity(intent);
			}
		});
		PromosiBT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("InformasiPromosiActivity", "Informasi button");
				try {
					String lokasi=getlocation2(activity);
					if(!lokasi.equals("not active"))
					{
						Intent intent = new Intent(activity, PromosiPenjualanActivity.class);
						intent.putExtra("lokasi",lokasi);
						startActivity(intent);
					}
					else{
						Intent intent = new Intent(activity, LokasGeraiActivity.class);
						intent.putExtra("from","informasipromosi");
						startActivity(intent);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	public String getlocation2(Context context) throws IOException{
		String info="not active";
		// Acquire a reference to the system Location Manager
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		Location location;

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {

			public void onStatusChanged(String provider, int status, Bundle extras) {}

			public void onProviderEnabled(String provider) {}

			public void onProviderDisabled(String provider) {}

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub

			}
		};

		// Register the listener with the Location Manager to receive location updates
		if(locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER)){
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		}
		location=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		Log.d("", "getlocation 1"+info+"location= "+location);
		if(location!=null){
			Log.d("", "getlocation 2"+info);
			info=getlocation(location.getLatitude(), location.getLongitude(),context);
			Log.d("", "getlocation 3"+info);
		}

		Log.d("", "Lokasi = "+info);
		return info;
	}

	public String getlocation(double latitude, double longitude,Context context) throws IOException{//using longitude latitude
		String info="Lokasi Belum dapat ditemukan";
		Geocoder geocoder;
		List<Address> addresses;
		geocoder = new Geocoder(context, Locale.getDefault());

		addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

		String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
		String city = addresses.get(0).getLocality();
//		String state = addresses.get(0).getAdminArea()+"(provinsi)";
		String state = addresses.get(0).getAdminArea();
		String country = addresses.get(0).getCountryName();
		String postalCode = addresses.get(0).getPostalCode();
		String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

//		info=address+"\n"+city+"\n"+state+"\n"+country+"\n"+postalCode+"\n"+knownName;
		info=state;
		return info;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.informasi_promosi, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
