package com.example.informasipromosi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;

public class PromosiPenjualanActivity extends Activity {
    TextView titletv,penjelasantv;
    Button eventBT,spesialBT;
    Activity activity;
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promosi_penjualan);
        titletv = (TextView)findViewById(R.id.textView);
        penjelasantv = (TextView)findViewById(R.id.textView1);
        Intent intent=getIntent();
        title = intent.getStringExtra("lokasi");
        titletv.setText("PROMOSI\nPENJUALAN\n"+title);
        eventBT = (Button)findViewById(R.id.button1);
        spesialBT = (Button)findViewById(R.id.button2);
        activity=this;
        penjelasantv.setText(CustomHttpClient.info(this,"penjelasanpromosidanpenjualan.php"));
        eventBT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("PromosiPenjualan", "event button");
                Intent intent = new Intent(activity, PromosiSpesialActivity.class);
                intent.putExtra("from","event");
                intent.putExtra("lokasi",title);
                startActivity(intent);
            }
        });
        spesialBT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("PromosiPenjualan", "spesial button");
                Intent intent = new Intent(activity, PromosiSpesialActivity.class);
                intent.putExtra("from","spesial");
                intent.putExtra("lokasi",title);
                startActivity(intent);
            }
        });
    }
}
