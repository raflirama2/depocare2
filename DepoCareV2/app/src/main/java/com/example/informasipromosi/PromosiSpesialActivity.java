package com.example.informasipromosi;

import android.app.Activity;
//import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.depocarev2.GPS;
import com.example.depocarev2.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PromosiSpesialActivity extends Fragment {
    TextView titleTV;
    ListView list;
    Integer[] imageId = {
            R.drawable.kdk1,
            R.drawable.kdk2,
            R.drawable.kdk3,
            R.drawable.kdk4,
            R.drawable.ariston1,
            R.drawable.ariston2,
            R.drawable.ariston3,
            R.drawable.ariston4,
            R.drawable.amstat1,
            R.drawable.amstat2,
            R.drawable.amstat3,
            R.drawable.amstat4,

    };
    Spinner spinner1;
    String title,from;
    Activity activity;
    GPS gps;

    public PromosiSpesialActivity(Activity activity,String from){
        this.from=from;
        this.activity=activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_promosi_spesial);
        View rootView = inflater.inflate(R.layout.activity_promosi_spesial, container, false);
        spinner1 = (Spinner)rootView.findViewById(R.id.spinner1);
        List<String> list_lokasi = new ArrayList<String>();
        list_lokasi.add("Jakarta/Jawa Barat");
        list_lokasi.add("Jawa Timur");
        list_lokasi.add("Bali");
        list_lokasi.add("Pilih Region");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, list_lokasi);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
//        titleTV=(TextView)rootView.findViewById(R.id.textView1);
        gps = new GPS();
        try {
            title= gps.getlocation2(getActivity());
            if(title.equals("not active")){
                spinner1.setSelection(3,true);

            }
            else if (title.equals("Jawa Timur")||title.equals("East Java")){
                spinner1.setSelection(1,true);
            }
            else if (title.equals("Jawa Barat")||title.equals("West Java")){
                spinner1.setSelection(0,true);
            }
            else if (title.equals("Bali")){
                spinner1.setSelection(2,true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Intent intent = getIntent();
//        String from = intent.getStringExtra("from");
//        title = intent.getStringExtra("lokasi");

        if(from.equals("event")){
//            titleTV.setText("EVENT PROMOSI\nBERLANGSUNG\n"+title);
        }
        else if (from.equals("spesial")){
//            titleTV.setText("PROMOSI\nSPESIAL\n"+title);
        }
        CustomListItem adapter = new CustomListItem(getActivity(),  imageId);
        list=(ListView)rootView.findViewById(R.id.listView1);
        list.setAdapter(adapter);


//        Toast.makeText(getApplicationContext(), "Setress", Toast.LENGTH_SHORT).show();
        return rootView;
    }
}
