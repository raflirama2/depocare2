package com.example.informasipromosi;

import java.util.Locale;

//import android.support.v7.app.ActionBarActivity;

import com.example.depocarev2.R;
import com.example.depocarev2.R.id;
import com.example.depocarev2.R.layout;
import com.example.depocarev2.R.menu;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class LokasiGeraiDBActivity extends Activity {
	Button gotomapsBT;
	ImageView img,img2;
	TextView titleTV,valueTV;	
	Activity activity;
	LokasGeraiActivity LG;
	double latitude = 0;
	double longitude = 0;
	String mTitle = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lokasi_geraidb);
		titleTV=(TextView)findViewById(R.id.textView1);
		valueTV=(TextView)findViewById(R.id.textView2);
		activity=this;
		img = (ImageView)findViewById(R.id.imageView1);
		img2=(ImageView)findViewById(R.id.img);
		
		Intent intent = getIntent();
//		String pilih="0";
//		pilih= intent.getStringExtra("lokasi").toString();
		int pilih2 = intent.getIntExtra("lokasi", 100);
		LG=new LokasGeraiActivity(activity,"");
		if(pilih2==0){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nKALIMALANG - JAKARTA TIMUR");
			valueTV.setText(LG.web[0]);
			img.setImageResource(R.drawable.map_depo_kalimalang);
			latitude = -6.2475008;
			longitude= 106.920672;
			mTitle = "DEPO BANGUNAN KALIMALANG";
			img2.setImageResource(R.drawable.depo_kalimalang);
		}
		else if(pilih2==1){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nTANGERANG - TANGERANG SELATAN");
			valueTV.setText(LG.web[1]);
			img.setImageResource(R.drawable.map_depo_tangerang);
			latitude = -6.2407174;
			longitude = 106.6493279;
			mTitle= "DEPO BANGUNAN TANGERANG";
			img2.setImageResource(R.drawable.depo_tangerang);
		}
		else if(pilih2==2){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nSIDOARJO - JAWA TIMUR");
			valueTV.setText(LG.web[2]);
			img.setImageResource(R.drawable.map_depo_gedangan);
			latitude=-7.3870732;
			longitude= 112.7269496;
			mTitle="DEPO BANGUNAN GEDANGAN";
			img2.setImageResource(R.drawable.depo_sidoarjo);
		}
		else if(pilih2==3){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nMALANG - JAWA TIMUR");
			valueTV.setText(LG.web[3]);
			img.setImageResource(R.drawable.map_depo_malang);
			latitude = -7.9124853;
			longitude = 112.6545007;
			mTitle = "DEPO BANGUNAN MALANG";
			img2.setImageResource(R.drawable.depo_malang);
		}
		else if(pilih2==4){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nBANDUNG - JAWA BARAT");
			valueTV.setText(LG.web[4]);
			img.setImageResource(R.drawable.map_depo_bandung);
			latitude = -6.9395498;
			longitude=107.6712919;
			mTitle="DEPO BANGUNAN BANDUNG";
			img2.setImageResource(R.drawable.depo_bandung);
		}
		else if(pilih2==5){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nDENPASAR - BALI");
			valueTV.setText(LG.web[5]);
			img.setImageResource(R.drawable.map_depo_denpasar);
			latitude = -8.674295;
			longitude=115.1836986;
			mTitle = "DEPO BANGUNAN DENPASAR";
			img2.setImageResource(R.drawable.depo_denpasar);
		}		
		else if(pilih2==6){
			titleTV.setText("LOKASI GERAI\nDEPO BANGUNAN\nBOGOR - BOGOR");
			valueTV.setText(LG.web[6]);
			img.setImageResource(R.drawable.map_depo_bogor);
			latitude = -6.5612576;
			longitude=106.7966423;
			mTitle="DEPO BANGUNAN BOGOR";
			img2.setImageResource(R.drawable.depo_bogor);
		}		
		else{
			titleTV.setText("Belum Terdefinisi");
			valueTV.setText("");
		}
		img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("InformasiActivity", "Tentang button");
//				String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
				String uri ="http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + mTitle + ")";
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lokasi_gerai, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
