package com.example.informasipromosi;

//import android.support.v7.app.ActionBarActivity;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.id;
import com.example.depocarev2.R.layout;
import com.example.depocarev2.R.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class InformasiActivity extends Activity {
	
	Button tentangBT,lokasiBT;
	Activity activity;
	TextView penjelasanTV;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_informasi);
		activity= this;
		tentangBT=(Button)findViewById(R.id.button1);
		lokasiBT=(Button)findViewById(R.id.button2);
		penjelasanTV = (TextView)findViewById(id.textView2);
		penjelasanTV.setText(CustomHttpClient.info(this,"penjelasaninformasi.php"));
		tentangBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("InformasiActivity", "Tentang button");
				Intent intent = new Intent(activity, TentangActivity.class);
				startActivity(intent);
			}
		});
		lokasiBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("InformasiActivity", "lokasi button");
				Intent intent = new Intent(activity, LokasGeraiActivity.class);
				intent.putExtra("from","informasi");
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.informasi, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
