package com.example.informasipromosi;

import com.example.depocarev2.CustomHttpClient;
import com.example.depocarev2.R;
import com.example.depocarev2.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TentangActivity extends Fragment {
	TextView penjelasanTV;
	Activity activity;
	public TentangActivity(Activity activity){
		this.activity=activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(layout.activity_tentang, container, false);
		penjelasanTV = (TextView)rootView.findViewById(R.id.textView2);
		penjelasanTV.setText(CustomHttpClient.info(activity,"penjelasantentang.php"));

		return rootView;
	}

}
