package com.example.informasipromosi;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.depocarev2.R;

public class CustomListItem extends BaseAdapter{

	private Context mContext;

	// Keep all Images in array
	public Integer[] mThumbIds;

	// Constructor
	public CustomListItem(Context c,Integer[] mThumbIds){
		mContext = c;
		this.mThumbIds=mThumbIds;
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//		layoutParams.gravity= Gravity.CENTER;
//		layoutParams.bottomMargin=15;
		layoutParams.setMargins(15,15,15,15);
		ImageView imageView = new ImageView(mContext);
		imageView.setImageResource(mThumbIds[position]);
		imageView.setScaleType(ImageView.ScaleType.FIT_END);
		imageView.setLayoutParams(layoutParams);
		imageView.setBackgroundResource(R.drawable.rounded_corner2);
		return imageView;
	}

}
